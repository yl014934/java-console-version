/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dronesm;

/**
 *
 * @author aaa
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class HoverArena extends JPanel{
    DroneArena drone;
    HoverArena() {
        //... Create components
        drone = new DroneArena();        
        JButton startButton = new JButton("Start");        
        JButton stopButton  = new JButton("Stop");
        
        //... Add Listeners
        startButton.addActionListener(new StartAction());
        stopButton.addActionListener(new StopAction());
        
        //... Layout inner arena with two buttons horizontally
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.add(startButton);
        buttonPanel.add(stopButton);
        
        //... Layout outer arena with button panel above bouncing ball
        this.setLayout(new BorderLayout());
        this.add(buttonPanel, BorderLayout.NORTH);
        this.add(drone       , BorderLayout.CENTER);
    }//end constructor
    
    
    ////////////////////////////////////// inner listener class StartAction
    class StartAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            drone.setAnimation(true);
        }
    }
    
    
    //////////////////////////////////////// inner listener class StopAction
    class StopAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            drone.setAnimation(false);
        }
    }
}
